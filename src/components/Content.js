import Part from "./Part";

const  Content=(props) =>{
    return (
        <div>
            {
            props.parts.map((part, index) => {
                return (
                    <Part nombre={part.name} numero={part.exercises} ></Part>
                );
             })
            }

        </div>
    )
}

export default Content
