import Part from "./Part";

const  Total=(props) =>{

    let totalExercises=0;

    return (
        <div>
            {
                props.parts.map((part, index) => {
                    totalExercises+=part.exercises
                })
            }

            <p>Number of exercises {totalExercises}</p>
        </div>
    )
}

export default Total
